if string.match(get_window_name(), "cora_webapp") then
  set_window_workspace(2);
end
if string.match(get_window_name(), "docker%-test") then
  set_window_workspace(3);
end
if string.match(get_window_name(), "docker_test") then
  set_window_workspace(4);
end
if string.match(get_window_name(), "cora ") then
  set_window_workspace(5);
end
if string.match(get_window_name(), "csc") then
  set_window_workspace(6);
end
if string.match(get_window_name(), "YouTube") then
  undecorate_window();
  debug_print("YT Window: " .. get_window_name());
end
if string.match(get_window_name(), "Netflix") then
  undecorate_window();
  debug_print("Netflix window: " .. get_window_name());
end

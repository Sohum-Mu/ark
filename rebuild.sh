#!/bin/bash
###YOUDO:
#git clone https://Sohum-Mu@bitbucket.org/Sohum-Mu/ark.git ~/ark && \
#cd ~/ark && \
#cp .bash_aliases ~/.bash_aliases && \
#source ~/.bash_aliases && \

###MUDO:
# MAIN
sudo apt-get update -y && \
sudo apt-get install -y python3-pip && \
sudo apt-get install  -y devilspie2 && \
sudo apt-get install  -y xserver-xephyr && \

#GH
curl -fsSL https://cli.github.com/packages/githubcli-archive-keyring.gpg | sudo gpg --dearmor -o /usr/share/keyrings/githubcli-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/githubcli-archive-keyring.gpg] https://cli.github.com/packages stable main" | sudo tee /etc/apt/sources.list.d/github-cli.list > /dev/null
sudo apt update
sudo apt install gh

#CODE
sudo apt-get install -y code

#CHROME
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
sudo apt install -y ./google-chrome-stable_current_amd64.deb && \

#PYVIM
sudo apt-get install -y vim && \
sudo apt-get install -y git && \
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim && \
git clone https://Sohum-Mu@bitbucket.org/Sohum-Mu/ark.git ~/ark && \
cd ~/ark && \
cp .vimrc ~/.vimrc

#DOCKER
sudo apt-get remove docker docker-engine docker.io containerd runc && \
sudo apt-get update && \
sudo apt-get install -y \
apt-transport-https \
ca-certificates \
curl \
gnupg \
lsb-release && \
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
echo \
"deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null && \
sudo apt-get update -y && \
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER && newgrp docker

#DOCKER-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
sudo chmod +x /usr/local/bin/docker-compose

#KUBERNETES
sudo apt-get install -y kubernetes && \
udo apt-get install -y apt-transport-https ca-certificates curl && \
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg && \
177         echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list &&     \
sudo apt-get update -y && \
sudo apt-get install -y kubectl && \
kubectl completion bash && \
echo "source <(kubectl completion bash)" >>~/.bashrc && \
sudo apt-get install -y kubelet
echo ==========================================================================================================
echo NOTE: Kubectl requires the user to run: sudo su: kubectl completion bash >/etc/bash_completion.d/kubectl =
echo ==========================================================================================================
		
#KUBECTL-convert	
curl -LO https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl-convert && \
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl-convert.sha256" && \
echo "$(<kubectl-convert.sha256) kubectl-convert" | sha256sum --check && \
sudo install -o root -g root -m 0755 kubectl-convert /usr/local/bin/kubectl-convert && \
kubectl convert --help

#KIND
cd /home/mu/Documents/github && \
git clone https://github.com/kubernetes-sigs/kind.git && \
cd /home/mu/Documents/github/kind
make build
sudo mv bin/kind /usr/bin/kind && \
echo Kind located at: &&\
which kind

#MINIKUBE
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && \
sudo install minikube-linux-amd64 /usr/local/bin/minikube && \
minikube config set driver docker

#KUBELET/KUBEADM/KUBECTL
sudo apt-get update -y && \
sudo apt-get install -y apt-transport-https ca-certificates curl && \
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg && \
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list &&     \
sudo apt-get update -y && \
sudo apt-get install -y kubelet kubeadm kubectl && \
sudo apt-mark hold kubelet kubeadm kubectl

#AWS
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \ 
unzip awscliv2.zip && sudo ./aws/install
#AWS-cli
sudo ./aws/install --bin-dir /usr/local/bin --install-dir /usr/local/aws-cli --update
#AWS-docker-cli
curl -L https://raw.githubusercontent.com/docker/compose-cli/main/scripts/install/install_linux.sh | sh && \
sudo mv docker-linux-amd64 docker && \
sudo chmod +x docker
#AWS-ecs-cli
sudo curl -Lo /usr/local/bin/ecs-cli https://amazon-ecs-cli.s3.amazonaws.com/ecs-cli-linux-amd64-latest && \
sudo chmod +x /usr/local/bin/ecs-cli && ecs-cli --version
#AWS-eb-ecli
yes | pip install awsebcli --upgrade --user
yes | sudo pip install ds4drv

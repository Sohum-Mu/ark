#<! Installation !>#
alias testing='echo ahem...1, 2, 3,...'
###BOOT SEQUENCE###
#BOOT
alias start='code & \
	zoom & \
	firefox &'
alias pie='devilspie2'
alias start-home='xrandr --output eDP-1-1 --off &&\
	code & \
	zoom & \
	google-chrome &'
#ALIAS
alias setalias='vim ~/.bash_aliases'
alias getalias='source ~/.bash_aliases'

########## WORK ##########

####MISC####
#CHROME
alias chrome='google-chrome'
alias ports='sudo lsof -i -P -n | grep LISTEN'
###apt utilities###
alias get='sudo apt install'
alias update='sudo apt update'
alias upgrade='sudo apt upgrade'
alias fupgrade='sudo apt full-upgrade'
alias purge='sudo apt purge'
alias autorm='sudo apt autoremove'
alias remove='sudo apt remove'
alias fupdate='sudo apt update && sudo apt full-upgrade'

alias ml-tower='ssh fireball@192.168.8.117'
alias ml-notebook='jupyter-notebook --ip $MLTOWER --port 8888'

alias export-os='sudo dpkg --get-selections | sed "s/.*deinstall//" | sed "s/install$//g" > ~/pkglist'
#COPY $HOME directory for config
alias import-os='sudo apt-get install aptitude
sudo aptitude update && cat pkglist | xargs sudo aptitude install -y'


###Python utilities###
alias py='python3'
alias pip='pip3'
#venv, follow with venv path/name
alias venv='python3 -m venv'
alias pipi='pip3 install'
alias pipu='pip3 uninstall'
alias cora-wheel='sudo rm -R ./dist && \
	python3 setup.py bdist_wheel && \
	cp -a ./dist/* ~/Documents/github/fast-overhaul'

###Directory utilities###
alias up='cd ..'
alias ark='cd /home/mu/Documents/github/ark'
alias board='cp /home/mu/.bash_aliases /home/mu/Documents/github/ark/.bash_aliases && \
	cd /home/mu/Documents/github/ark && \	
	git status && git add . && git commit && git push'
alias unboard='echo MAKE SURE YOU ARE IN THE ark/ DIRECTORY && \
	echo user: && \
	read user && \
	git pull && \
	cp ./.bash_aliases /home/$user/.bash_aliases && \
	source /home/$user/.bash_aliases && \
	echo Done.
	'
alias extract='echo File: && \ 
	read file && \
	tar -xvzf $file'
alias ls='ls -la'
alias gitdir='cd Documents/github'
alias search='grep --color=auto'
alias sr-tools='cd ~/Documents/github/sr_tools'
alias cora-tools='cd ~/Documents/github/cora'
alias cora-webapp='cd ~/Documents/github/cora_webapp'
alias cora-test='cd ~/Documents/github/docker-test'
alias cora_test='cd ~/Documents/github/docker_test'
alias fast='cd /home/mu/Documents/github/fast-overhaul'
alias faster='gnome-terminal --working-directory="/home/mu/Documents/github/fast-overhaul" exit 0' 
alias f4st='/home/mu/Documents/github/ark/faster.sh'
alias fastest='/home/mu/Documents/github/ark/fastest.sh'
alias ml-cp='echo Path from: && \
	read from && \
	echo Path to && \
	read to && \
	sudo scp fireball@192.168.8.117:$from $to'
alias mount-remote='echo ml-tower:192.168.8.117 && \
		echo User: && \
		read user && \
		echo Remote IP: && \
		read remote && \
		echo Dir: && \
		read dir && \
		sudo apt-get install sshfs && \
		sudo mkdir /mnt/$dir && \
		sudo sshfs -o allow_other,default_permissions $user@$remote:/ /mnt/$dir && \
		ls /mnt/$dir'

###git utilities###
alias clone='git clone'
alias status='git status'
alias push='git push'
alias pull='git pull'
alias commit='git commit -m'
alias adda='git add .'
alias save='git status && git add . && git commit && git push'
alias switch='git checkout'
alias newb='echo New branch name: && \
	read branchto && \
	echo From branch: && \
	read branchfrom && \
	git checkout -b $branchto $branchfrom && \
	touch $branchto.txt && \
	git add . && \
	git commit -m "Initialising $branchto" && \
	git push --set-upstream origin $branchto'

alias merge='echo Branch to: && \
	read branchto && \
	echo Branch from: && \
	read branchfrom && \
	git checkout $branchto && \
	git merge --no-ff $branchfrom'

alias gitz='git archive -o cora.zip HEAD'
alias gitct='git checkout docker_test && \
	git pull && \
	git checkout docker-test && \
	git merge --no-ff docker_test && \
	git push'
alias gitcu='git checkout docker-test && \
	git pull && \
	git checkout docker-build
	git merge --no-ff docker-test && \
	git push'
alias _test='git checkout docker_test'
alias test='git checkout docker-test'
alias build='git checkout docker-build'
alias g1t='echo Branch name: && \ 
	read branch && \
	git clone --depth 1 https://Sohum-Mu@bitbucket.org/NEORA-Admin/cora_webapp.git --branch $branch --single-branch'

####Kubernetes####
###Setup###
###UTILS###
# alias set-pod-name='export POD_NAME=$(kubectl get pods -o go-template --template "{{range .items}}{{.metadata.name}}{{'\n'}}{{end}}")
# echo Name of the Pod: $POD_NAME'
# alias set-node-port='export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
# echo NODE_PORT=$NODE_PORT'
alias view-prod='kubectl logs -l app=api --max-log-requests 360 --ignore-errors --all-containers -f --namespace fuego-detect'

####Docker utilities####
###Regular###
alias docker='sudo docker'
alias docker-compose='sudo docker-compose'
alias dcompose='sudo docker compose'
alias compose='sudo docker-compose'
alias dev-up='sudo docker-compose -f docker-compose.yml up'
alias dev-build='sudo docker-compose -f docker-compose.yml up --build'
alias dev-down='sudo docker-compose -f docker-compose.yml down -v'
alias dev-rb='sudo docker-compose -f docker-compose.yml down -v && sudo docker system prune -a && sudo docker-compose -f docker-compose.yml up --build'
alias dlsi='sudo docker image ls'
alias dprune='sudo docker system prune -a'
alias drmi='sudo docker rmi -f'

####CORA####
alias cora='clone https://Sohum-Mu@bitbucket.org/NEORA-Admin/cora_webapp.git ~/Documents/github'
alias cora-b='sudo docker build -t neora0docker/webapp:latest .'
alias cora-p='sudo docker push neora0docker/webapp:latest'
alias cora-rb='sudo docker system prune -a && sudo docker build -t neora0docker/webapp:latest . && sudo docker push neora0docker/webapp:latest'
alias fast-rb='sudo docker rmi -f fast-server && \
	sudo docker rmi -f fast-webapp && \
	sudo docker rmi -f fast-database && \
	sudo docker rmi -f fast-react && \
	sudo docker-compose up'
alias cora-upd='sudo docker-compose -f docker-compose.yml exec webapp python prod.py update_db'
alias cora-cdb='sudo chmod -R 777 /usr/Data/postgres-data/ && \
	rm -r /usr/Data/postgres-data'
alias cora-cd='sudo rm -r appdata/'
alias cora-ca='sudo rm -r migrations/ && sudo rm -r postgres-data/ && sudo rm appdata/'
alias cora-wpt='sudo docker attach webapp'
alias cora-wt='sudo docker exec -it webapp bash'
alias cora-dbt='sudo docker exec -it db bash'

### RUNNING CONTAINER UPDATE FUNCTIONS
## ROUTES
#main
alias cora-main='cd ~/Documents/github/cora_webapp/app/main && \
	echo File to update: && \
	read file && \
	sudo docker cp ./$file webapp:/app/main/$file'
#users
alias cora-users='cd ~/Documents/github/cora_webapp/app/users && \
	echo File to update: && \
        read file && \
        sudo docker cp ./$file webapp:/app/users/$file'
#analytics
alias cora-analytics='cd ~/Documents/github/cora_webapp/app/analytics && \
	echo File to update: && \
        read file && \
        sudo docker cp ./$file webapp:/app/analytics/$file'
#errors
alias cora-errors='cd ~/Documents/github/cora_webapp/app/errors && \
	echo File to update: && \
        read file && \
        sudo docker cp ./$file webapp:/app/errors/$file'
#posts
alias cora-posts='cd ~/Documents/github/cora_webapp/app/posts && \
	echo File to update: && \
        read file && \
        sudo docker cp ./$file webapp:/app/posts/$file'
#templates
alias cora-templates='cd ~/Documents/github/cora_webapp/app/templates && \
	echo File to update: && \
        read file && \
        sudo docker cp ./$file server:/etc/nginx/html/$file'
#prod
alias cora-prod='cd ~/Documents/github/cora_webapp && \
	sudo docker cp ./prod.py webapp:/app/prod.py'

# Fullpath Update
alias docker-fu='read file && \
	read file2 && \
	read cont &&\
	sudo docker cp $file $cont:/$file2' 
	

####AWS utilities####
##Repository###
alias aws-create-repo='aws ecr create-repository --image-scanning-configuration scanOnPush=true --region us-east-1 --repository-name'
alias aws-dtag='docker tag webapp:0.1.8.19 138626108157.dkr.ecr.us-east-1.amazonaws.com/webapp:latest'
alias aws-dpush='docker push 138626108157.dkr.ecr.us-east-1.amazonaws.com/webapp:latest'
alias aws-dpull='docker pull 138626108157.dkr.ecr.us-east-1.amazonaws.com/webapp:latest'

###Credentials setting###
##Docker##
alias aws-set-pass='aws ssm put-parameter --name USER --type String --value'
alias aws-set-user='aws ssm put-parameter --name PASSWD --type String --value'

###CORA###
#Test
alias aws-test-up='eb init -p docker cora && \
	eb create -i t2.xlarge cora-test --timeout 60 -k x1'
alias aws-test-down='eb terminate cora-test'
alias aws-test-rb='eb terminate cora-test && \
	eb init -p docker cora && \
	eb create -i t2.xlarge cora-test --timeout 60 -k x1'
#Deploy
alias aws-up='eb init -p docker cora && \
	eb create -i t2.xlarge cora-env --timeout 60 -k x1'
#Terminate
alias aws-down='eb terminate cora-env'
#Redeploy
alias aws-rb='eb terminate cora-env && \
	eb init -p docker cora && \
	eb create -i t2.xlarge cora-env --timeout 60 -k x1'
#patch
alias aws-patch='read ip && \
	read file && \
	scp -i /home/mu/.ssh/x1 $file ec2-user@$ip:/usr/Data'
alias git-patch-init='git clone --depth 1 https://Sohum-Mu@bitbucket.org/NEORA-Admin/cora_webapp.git --branch docker-patch --single-branch'
alias live-patch='cp -a /cora_webapp/. /'
alias gitpatch='git clone --depth 1 https://Sohum-Mu@bitbucket.org/NEORA-Admin/cora_webapp.git --branch docker-patch --single-branch && \
		cp -a /cora_webapp/. /'
alias dump-wa-schema='pg_dump -U postgres -f /var/lib/postgresql/data/backup/webapp.dump --format=custom -n webapp sso_db'
alias restore-wa-schema='pg_restore -c -U postgres -d sso_db /var/lib/postgresql/data/backup/webapp.dump'

alias rds-dump-wa='pg_dump --host=cora-database.cmhvzt9uxpqj.us-west-2.rds.amazonaws.com --port=5432 \
  --format=custom \
  -U postgres --password --dbname=sso_db --schema webapp \
  >webapp.dump'
alias rds-rest-wa='pg_restore -c --host=cora-database.cmhvzt9uxpqj.us-west-2.rds.amazonaws.com --port=5432 \
  --verbose --exit-on-error --single-transaction \
  -U postgres --password --schema=webapp \
  --dbname=sso_db \
  ./webapp_mar292021.dump'
alias rds-cu='createuser --host=rds.example.com --port=5432 \
  --username=rds_master \
  --no-createdb --no-createrole --no-superuser \
  --login --pwprompt \
  db_test_user'

#Suspend (terminate instances)
alias aws-suspend='eb eb scale 0'
#Complete rebuild
alias rebuild='sudo docker system prune -a && \
	sudo docker build -t neora0docker/webapp:latest . && \
	sudo docker push neora0docker/webapp:latest && eb init -p docker cora && \
	eb create cora-env'
#Health
alias health='eb health --refresh cora-env'
alias log-engine='eb logs --log-group /aws/elasticbeanstalk/cora-env/var/log/eb-engine.log --stream'
alias log-ebhooks='eb logs --log-group /aws/elasticbeanstalk/cora-env/var/log/eb-hooks.log --stream'
alias log-docker='eb logs --log-group /aws/elasticbeanstalk/cora-env/var/log/docker --stream'
alias log-docker-events='eb logs --log-group /aws/elasticbeanstalk/cora-env/var/log/docker-events.log --stream'
alias log-webapp='eb logs --log-group /aws/elasticbeanstalk/cora-env/var/log/eb-docker/containers/eb-current-app/stdouterr.log --stream'
alias log-nginx-access='eb logs --log-group /aws/elasticbeanstalk/cora-env/var/log/nginx/access.log.log --stream'
alias nginx-error='eb logs --log-group /aws/elasticbeanstalk/cora-env/var/log/nginx/error.log --stream'

#Test-health
alias thealth='eb health --refresh cora-test'
alias tlog-engine='eb logs --log-group /aws/elasticbeanstalk/cora-test/var/log/eb-engine.log --stream'
alias tlog-ebhooks='eb logs --log-group /aws/elasticbeanstalk/cora-test/var/log/eb-hooks.log --stream'
alias tlog-docker='eb logs --log-group /aws/elasticbeanstalk/cora-test/var/log/docker --stream'
alias tlog-docker-events='eb logs --log-group /aws/elasticbeanstalk/cora-test/var/log/docker-events.log --stream'
alias tlog-webapp='eb logs --log-group /aws/elasticbeanstalk/cora-test/var/log/eb-docker/containers/eb-current-app/stdouterr.log --stream'
alias tlog-nginx-access='eb logs --log-group /aws/elasticbeanstalk/cora-test/var/log/nginx/access.log.log --stream'
alias tnginx-error='eb logs --log-group /aws/elasticbeanstalk/cora-test/var/log/nginx/error.log --stream'
#Terminal
alias cora-ebt='eb ssh'
######### FAST.AI  #########
alias learn-setup='cd ~/Documents/github/fastbook && \
	sudo tensorman run --gpu --root --python3 --jupyter --name fastai_learn bash'
	#INBASH: 
	# pip install --upgrade pip && \
	# pip install torch==1.8.1 && \
	# apt-get update && \
	# apt-get install graphviz && jupyter notebook --allow-root --ip=0.0.0.0 --no-browser 
alias learn-save='tensorman save fastai_learn fastai_learn'
	
alias learn='cd ~/Documents/github/fastbook && \
	sudo tensorman "=fastai_learn" run --gpu --root bash'
########## UNREAL ##########
##SETUP###
alias build-unreal='cd Documents/github && \
	git clone https://github.com/EpicGames/UnrealEngine.git && \
	cd UnealEngine/ && \
	./Setup.sh && \
	./GenerateProjectFiles.sh && \
	make'
#EDIT
alias ue='cd ~/Documents/github/UnrealEngine/Engine/Binaries/Linux && ./UE4Editor'



############ UNI ############

####CSC2408####
###asciinema###
alias record='asciinema rec u1078587_a1_q3.cast'

####CSC2408####
###g++###
alias compile='g++ input.cpp -o output.exe'
alias execute='./output.exe'
alias rung='g++ input.cpp -o output.exe && ./output.exe'

########### PERSONAL ##########
alias youtube='google-chrome --kiosk --app=https://youtube.com'
alias yt='echo Enter the video ID: && \
	read vid && \
	chrome --kiosk --app=https://youtube.com/watch_popup?$vid'
alias media-home='Xephyr -ac -resizeable -br -reset -terminate 2> /dev/null :2 & DISPLAY=:2.0'
alias media='Xephyr -ac -resizeable -br -reset -terminate 2> /dev/null :1 & DISPLAY=:1.0'alias yuzu='cd ~/ && \
	./yuzu.AppImage'
